﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstSoln
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var app = new Menu(new Logger());
            app.Run();

        }

        /*static void EnrollUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("-----Enrolling User-----");
            userService.Register(new RegisterViewModel{FirstName = "Junior", LastName = "Nwokolo", Email = "junior.sage@omekannaya.com", Username = "Sage",
                Birthday = new DateTime(2000, 01, 22), Password = "1234@one", ConfirmPassword = "1234@one"});
            
        }

        static void LoginUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("-----Logging in User------");
            userService.Login(new LoginViewModel { UsernameEmail = "Sage", Password = "1234@one" });
        }

        static void DeleteUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("----Deleting user------");
            userService.Delete(3);
            Console.WriteLine("Deleted user with that Id");
        }

        static void GetUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("-----Get a single user-------");
            userService.Get(6);
        }

        static void UpdateUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            Console.WriteLine("----Updating a User-------");
            userService.Update(new UpdateViewModel { Username = "Ronaldo", Email = "junior.sage@omekannaya.com", ConfirmNewPassword = "Character" });
        }*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Interfaces
{
    public interface ILogger
    {
        void Log(string text);
        void Clear();
    }
}

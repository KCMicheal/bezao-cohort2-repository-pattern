﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Helpers
{
    public class Validate
    {
        public static string CheckNull(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return $"{text} cannot be empty";
            return text;
        }
        public static bool CheckIfNull(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return true;
            return false;
        }
    }
}

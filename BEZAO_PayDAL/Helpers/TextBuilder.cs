﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Helpers
{
    public class TextBuilder
    {
        private static StringBuilder sb;

        public static String DisplayMenu()
        {
            sb = new StringBuilder().AppendLine();
            sb.Append("Welcome to the CY Bank's v2.0");
            sb.AppendLine();
            sb.Append("****************************");
            sb.AppendLine();
            sb.Append("Please choose an option");
            sb.AppendLine();
            sb.Append("1. Create an Account");
            sb.AppendLine();
            sb.Append("2. Make a Deposit");
            sb.AppendLine();
            sb.Append("3. Withdraw");
            sb.AppendLine();
            sb.Append("4. Check Balance");
            sb.AppendLine();
            sb.Append("5. Check All Customers Transaction history");
            sb.AppendLine();
            sb.Append("6. Exit");
            sb.AppendLine();
            sb.Append("****************************");


            return sb.ToString();
        }

        public static string Prompt(string text)
        {
            sb = new StringBuilder().AppendLine();
            sb.Append("+++++++++++++++++++++++++");
            sb.AppendLine();
            sb.Append(text);
            sb.AppendLine();
            sb.Append("+++++++++++++++++++++++++");

            return sb.ToString();
        }
    }
}

﻿using BEZAO_PayDAL.Helpers;
using BEZAO_PayDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Services
{
    public class Menu
    {
        private ILogger _logger;
        private string _menu;
        private string select;
        public Menu(ILogger logger)
        {
            _logger = logger;
            _menu = TextBuilder.DisplayMenu();
        }
        public void Run()
        {
            while (true)
            {
                _logger.Log(_menu);
                select = Console.ReadLine();
                if (MenuOptions(select))
                    break;
                _logger.Clear();
                _logger.Log("Invalid Input, try again!");
            }
            

        }
        private bool MenuOptions(string select)
        {
            BankActions choice = new BankActions(new Logger());
            switch (select)
            {
                case "1":
                    choice.CreateAccount();
                    return true;
                case "2":
                    choice.Deposit();
                    return true;
                case "3":
                    choice.Withdraw();
                    return true;
                case "4":
                    choice.CheckBalance();
                    return true;
                case "5":
                    choice.GetAllTransactions();
                    return true;
                case "6":
                    _logger.Log("Thank you for your time. Be sure to come again.");
                    return true;
                default:
                    _logger.Log("Invalid Input, try again!");
                    return false;
            }
        }

    }
}

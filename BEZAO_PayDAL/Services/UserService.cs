﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;

namespace BEZAO_PayDAL.Services
{
  public  class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Register(RegisterViewModel model)
        {

            /*if (!Validate(model))
            {
                return;
            }*/

            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = GetHashPassword(model.ConfirmPassword),
                Account = new Account{AccountNumber = 1209374652},
                Created = DateTime.Now
            };
            _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();
            Console.WriteLine("Success!");
        }

        public void Update(UpdateViewModel model)
        {
            var user = _unitOfWork.Users.Find(u => u.Email == model.Email).SingleOrDefault();
          
            user.Username = model.Username;
            user.Password = GetHashPassword(model.ConfirmNewPassword);
            _unitOfWork.Commit();
            Console.WriteLine("Updated User!");
        }

        public void Login(LoginViewModel model)
        {
            var loggedInUser = _unitOfWork.Users.Find(u => u.Username == model.UsernameEmail || u.Email == model.UsernameEmail && u.Password == u.Password).SingleOrDefault();
            Console.WriteLine($"{loggedInUser.Name}, {loggedInUser.Username} is logged in.");
        }

        public void Delete(int id)
        {
            var user = _unitOfWork.Users.Get(id);
            _unitOfWork.Users.Delete(user);
            _unitOfWork.Commit();

        }

        public void Get(int id)
        {
            var oneUser = _unitOfWork.Users.Find(u => u.Id == id).SingleOrDefault();
            Console.WriteLine($"{oneUser.Name}, {oneUser.Username}, {oneUser.Birthday}, {oneUser.Email}");
        }

        private bool Validate(RegisterViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email) 
                || string.IsNullOrWhiteSpace(model.FirstName) 
                || string.IsNullOrWhiteSpace(model.LastName) 
                || (model.Birthday == new DateTime()) 
                || (string.IsNullOrWhiteSpace(model.Password))
                || (model.Password != model.ConfirmPassword))
            {
                Console.WriteLine("A field is required");
                return false;

            }

            return true;

        }

        private static string GetHashPassword(string password)
        {
            using(var sha256 = SHA256.Create())
            {
                var hashedPass = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                return BitConverter.ToString(hashedPass).Replace("-", "").ToLower();
            }
        }
    }
}

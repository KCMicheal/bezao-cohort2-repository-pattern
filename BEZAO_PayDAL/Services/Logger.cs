﻿using BEZAO_PayDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL.Services
{
    public class Logger : ILogger
    {
        public void Log(string text)
        {
            Console.WriteLine(text);
        }
        public void Clear()
        {
            Console.Clear();
        }
    }
}

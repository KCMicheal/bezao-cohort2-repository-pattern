﻿using BEZAO_PayDAL.Helpers;
using BEZAO_PayDAL.Interfaces;
using BEZAO_PayDAL.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL
{
    public class FormInput
    {
        private readonly ILogger _logger;
        private string input;
        private string fName;
        private string lName;
        private string eMail;
        private DateTime birthday;
        private string userName;
        private string passWord;
        private string confirmPassword;
        private long accountNumber;
        private long amount;
        public FormInput(ILogger logger)
        {
            _logger = logger;
        } 
        public RegisterViewModel UserEnroll()
        {
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your FirstName:");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                fName = input;
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your LastName:");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                lName = input;
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your Email:");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                eMail = input;
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your Birthday in this format (yyyy,mm,dd)");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                 birthday = DateTime.Parse(input);
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your username");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                userName = input;
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            _logger.Clear();
            while (true)
            {
                _logger.Log("Please enter your password");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                passWord = input;
                _logger.Clear();
                _logger.Log("Field can't be empty");
            }
            while (true)
            {
                _logger.Log("Confirm your password");
                input = Console.ReadLine();
                if (!Validate.CheckIfNull(input))
                    break;
                confirmPassword = input;
                _logger.Clear();
                _logger.Log("Passwords don't match, try again!");
            }


            RegisterViewModel user = new RegisterViewModel()
            {
                FirstName = fName,
                LastName = lName,
                Email = eMail,
                Birthday = birthday,
                Username = userName,
                Password = passWord,
                ConfirmPassword = confirmPassword
            };

            return user;
        }



        
    }
}

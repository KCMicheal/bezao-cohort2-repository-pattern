﻿using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Helpers;
using BEZAO_PayDAL.Interfaces;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Repositories;
using Microsoft.EntityFrameworkCore;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAO_PayDAL
{
    public class BankActions
    {
        private readonly ILogger _logger;
       

        public BankActions(ILogger logger)
        {
            _logger = logger;
        }

        public void CreateAccount()
        {
            FormInput form = new FormInput(_logger);
            var user = form.UserEnroll();
            IUserService userService = new UserService(new UnitOfWorks(new BezaoPayContext()));
            userService.Register(user);
            Console.WriteLine("You've successfully signed up! Yay!!");
        }

        public void Deposit()
        {
            Console.WriteLine("Depositing");
        }

        public void Withdraw()
        {
            Console.WriteLine("Withdrawing");
        }
        
        public void CheckBalance()
        {
            Console.WriteLine("Checking Balance...");
        }

        public void GetAllTransactions()
        {
            Console.WriteLine("Getting all transactions...");
        }
    }
}
